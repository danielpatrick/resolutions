.PHONY: install local lint lint-fix flake8 black black-fix isort isort-fix bandit safety

install:
	poetry install

update:
	poetry update

local: install
	poetry run pre-commit install

lint: flake8 black isort

lint-fix: black-fix isort-fix

flake8:
	poetry run flake8

black:
	poetry run black --diff --check --preview .

black-fix:
	poetry run black --preview .

isort:
	poetry run isort --diff --check .

isort-fix:
	poetry run isort .

bandit:
	poetry run bandit -r src -q -n 3

safety:
	poetry export -f requirements.txt | poetry run safety check --stdin \
		-i 39642 # ignore reportlab for now
