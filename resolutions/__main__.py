import sys
from pathlib import Path

from .generator import PageGenerator


def readfile(filepath):
    with Path(filepath).open() as f:
        return [s.lower() for s in f.read().splitlines()]


if len(sys.argv) < 3:
    raise Exception("Required arguments: <inputfile> <outputfile>")


data = readfile(sys.argv[-2])
outfile = sys.argv[-1]

PageGenerator(data).write(outfile)
