import random

from reportlab.lib import colors
from reportlab.lib.enums import TA_CENTER
from reportlab.lib.pagesizes import A4, landscape
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import (
    BaseDocTemplate,
    Frame,
    LongTable,
    PageTemplate,
    Paragraph,
    TableStyle,
)


class PageGenerator:
    def __init__(self, data):
        self.data = data

        self.para_style = getSampleStyleSheet()["Normal"]
        self.para_style.alignment = TA_CENTER
        self.para_style.fontSize *= 1.3
        self.para_style.leading *= 1.3

    def random_data(self):
        data = random.sample(self.data, 25)

        return [
            [Paragraph(cell, self.para_style) for cell in data[i : i + 5]]
            for i in range(0, 25, 5)
        ]

    def create_table(self, width, height):
        tableStyle = [
            ("INNERGRID", (0, 0), (-1, -1), 0.25, colors.black),
            ("BOX", (0, 0), (-1, -1), 0.25, colors.black),
            ("ALIGNMENT", (0, 0), (-1, -1), "CENTER"),
            ("VALIGN", (0, 0), (-1, -1), "MIDDLE"),
            ("LEFTPADDING", (0, 0), (-1, -1), width / 50),
            ("RIGHTPADDING", (0, 0), (-1, -1), width / 50),
        ]

        table = LongTable(
            self.random_data(),
            colWidths=[width / 5] * 5,
            rowHeights=[height / 5] * 5,
        )
        table.setStyle(TableStyle(tableStyle))

        return table

    def write(self, filepath):
        doc = BaseDocTemplate(
            filepath,
            pagesize=landscape(A4),
            rightMargin=40,
            leftMargin=40,
            topMargin=40,
            bottomMargin=0,
            showBoundary=False,
        )

        frame = Frame(
            doc.leftMargin,
            doc.bottomMargin,
            doc.width,
            doc.height,
            id="normal",
        )

        template = PageTemplate(id="longtable", frames=frame)

        doc.addPageTemplates([template])

        doc.build([self.create_table(frame._width, frame._height * 0.9)])
