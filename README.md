# Resolutions

Generates a 5 x 5 "bingo card" in PDF format.

Accepts a file containing one item per line as input. Selects 25 at random and inserts them into the grid.

This is just a toy project to play with a PDF library to be honest.

## How to use

Using the example input at `examples.txt`:

```shell
$ make install
$ poetry run python -m resolutions examples.txt result.pdf
```

## Example output

![PDF screenshot](./example.png)
